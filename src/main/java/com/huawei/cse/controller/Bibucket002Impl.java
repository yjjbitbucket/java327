package com.huawei.cse.controller;


import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.apache.servicecomb.provider.rest.common.RestSchema;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.CseSpringDemoCodegen", date = "2019-01-25T06:21:40.291Z")

@RestSchema(schemaId = "bibucket002")
@RequestMapping(path = "/cse", produces = MediaType.APPLICATION_JSON)
public class Bibucket002Impl {

    @Autowired
    private Bibucket002Delegate userBibucket002Delegate;


    @RequestMapping(value = "/helloworld",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    public String helloworld( @RequestParam(value = "name", required = true) String name){

        return userBibucket002Delegate.helloworld(name);
    }

}
